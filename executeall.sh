#!/bin/sh

PYTHON="./venv/bin/python"

MAIN_DIR="src/main.py"

MAIN_MODULES=(
	"his"
	"his_adv"
	"bayes"
	"sw"
)

MAIN_TESTS=(
	""
	"-sameds"
	"-s 2 -r 10"
	"-s 5 -r 10"
	"-s 10 -r 10"
	"-ktest 75 -s 5 -r 10"
	"-ktest 50 -s 5 -r 10"
	"-k 75 -r 4 -rmodel"
	"-k 50 -r 4 -rmodel"
	"-top 500"
)

DATA_DIR="data/"

MAIN_FLAGS="-mt"

echo "Generating data"
${PYTHON} src/aggregate.py
${PYTHON} src/route.py

echo "Executing main module"
for MODULE in "${MAIN_MODULES[@]}"; do
	for TEST in "${MAIN_TESTS[@]}"; do
		${PYTHON} ${MAIN_DIR} -m ${MODULE} ${TEST} ${MAIN_FLAGS}
	done
	${PYTHON} src/csv_graphs_generator.py ${DATA_DIR}${MODULE}
done

