\clearpage\section{Results} \label{results}

{In this section, the generated results are observed and described, getting to conclusions on the different approaches
followed in this project. From the generated graphs, we could clearly see that there existed some correlation between an
observed victim and their browsing habits; even the worst performing attacker methods were able to somewhat relate all
different users with their traces, with varying degrees of success.}

{We applied the different attack approaches in different situations, simulating possible real cases. The modifications
could be simply described as losing or gaining information for every attack. They were:}
\begin{itemize}
\item {No changes done to the traces.}
\item {The attacker guessed on $t$ randomly selected traces at a time from the same user. All the accuracy scores for
every candidate were then added to generate an overall score.}
\item {The attacker guessed from the same portion of the dataset it created the model from. This could usually mean
higher accuracy overall, as the model was prepared from the same data.}
\item {The model and the guessing parts of the dataset were randomly selected, a $k$ percentage from the total. There was a
higher loss of data, with some users appearing in the guessing part not existing in the model. Might have represented a
late stage of the attack, where new actors have entered the scene and the data is obsolete.}
\item {Selecting a $k$ percentage, only from the guessing part.}
\item {Filtering only the top domains in Alexa's classification. The original dataset included the top $5000$ domains
in Germany.}
\end{itemize}

{Some attack were done multiple times, to reduce variation in random approaches.}

{The time of execution for all algorithms shown here did not truly reflect the complexity of the attack. They should be
only taken as an approximation of the execution in our program. The variation in time is mostly due to what part of the
software was written by us: in the case of the histogram algorithm, all its corresponding code was made for this project in
python, while the Naive Bayes machine learning guessing class comes from an existing library. We assumed that it ran faster
than what we could have done with our resources (for extreme cases, such as sequence alignment, we tried to optimize the
execution time as much as possible).}

{Another issue was the calculation of time when the execution is multi-threaded: all threads were executed at the same
time, each for every CPU core available. The time shown here was the measured execution time multiplied by the number of
cores. This approximation was not accurate: some threads usually ended earlier than others, which was not counted for here.
Because of this, it counted more time than it would truly take if it was executed in a single thread.}

{For each situation, we generated three graphs. We didn't write a commentary for all of them, as there were too many.
Instead, we looked at the most relevant for each situation. All of the information in its original state is available in
appendix A.}

{The generated graphs were:}
\begin{itemize}
\item {Top graph: percentage of top $1$, top $10$ and top $10\%$ guess occurrences for every user, ordered by their number
of training traces (how many traces a user had to build their model).}
\item {Correctness by user: Average candidate position for every user, ordered by their number of training traces. Lower is
better.}
\item {Correctness by trace length: Correctness for every trace, ordered by their number of domains.}
\end{itemize}

{We also generated information tables with Top percentages and accuracies. For each range of training traces, we calculated
their average value and their standard deviation.}

\subsection{Normal conditions}  \label{results:normal}

{On normal conditions, the Naive Bayes approach can be considered the one with the best performance, both in accuracy and
execution time. Sequence Alignment was the second best in guessing, but the worst in execution time, and histogram the last
with a processing time similar to Bayes.}

{For the Histogram approach, for users with over 30 training traces, it had a Top $1$ guess of around $25\%$, Top $10$ of
$45\%$ and Top $10\%$ of $70\%$. This set a rather low accuracy level (for 4 anonymized users, we could expect to find 1),
but it already hinted for some linkability inside a web click trace. Since this was the first approach we implemented, it
was reassuring to find some level of success. }

{Applying classification to the same approach (Advanced Histogram approach), it got higher classification accuracy. For its
average correctness, it tended to perform better than before, being under 200 for most of its users. For users with less
than 30 training traces, it tended to perform slightly worse, but nothing considerable (with approximately $1\%$ of
accuracy difference). Looking at these results, we could assume that trace classification was better for big amounts of
traces, where it would be harder to generate an accurate model.}

{The histogram attack was the only one we classified its traces, because it was the easiest one to modify and implement it.
We assumed that this improvement could have been seen for all other attacks.}

{The Sequence Alignment method got a better accuracy comparing it to the previous one. For Top $1$ it got a $34\%$, Top
$10$ a $57\%$ and Top $10\%$ a $87\%$, again for users with over 30 training traces. It seemed like the domain visit order
was in fact a valuable variable to distinct anonymized users to their web browsing habits.}

{However, its execution time was terrible: even with the improvements Cython brought, it took $345$ minutes to execute;
comparing it to the $188$ seconds execution time of the histogram ($302$ for the advanced approach) and the $167$ seconds
for Naive Bayes. Because of this, we didn't consider it to be a viable option if timing was a concern. We considered other
ways to improve the execution time, such as using machine learning to predict the local alignment, but this would have
fallen off the scope of this thesis.}

{Naive Bayes performance improvement can be seen by simply looking at its accuracy and correctness graphs. It had a Top
$1$ accuracy of over $60\%$, Top $10$ of over $78\%$ and Top $10\%$ of over $95\%$. This is the only approach that improved
greatly by having over 60 training traces; other methods usually got the same accuracy over 30 traces. This approach
had a high success rate: through it, an attacker could guess on the correct user for over half of the population (with
enough observed click sessions).}

{The main doubt was how it could have had a better accuracy if we were not taking into account domain order, like in
Sequence Alignment. If we compared it to the other naive approach (Histogram), there were two main differences: here we
calculated probabilities, instead of differences, and we took into account the frequency that a user appears $P(u)$, as a
user that had more traces had higher chances to appear. We believed that these two factors were the main contributors to
the better results, since probabilities are quite possibly better to calculate likeness. We could theorize another
approach combining the accuracy of Naive Bayes with the extra information of domain order; the main problem being its
execution time.}

{The processing time for this approach was strange, as it spent most of the time handling the data rather than the
guessing itself (handled by the scikit-learn library). It made more sense considering that the calculations done were not
too complex.}

{For users with a small quantity of training traces, it became harder to judge since they had more disperse values. Looking
at the standard deviation for all approaches, it can be seen how it increased the less traces we had.}

\begin{figure}[]
\label{fig:his_normal_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_his_normal_1.csv_top_graph.png}
\caption[Histogram Normal Accuracy]{\footnotesize{Histogram accuracy graph (normal conditions).}}
\end{figure}

\begin{figure}[]
\label{fig:bayes_normal_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_bayes_normal_1.csv_top_graph.png}
\caption[Bayes Normal Accuracy]{\footnotesize{Naive Bayes accuracy graph (normal conditions).}}
\end{figure}

\begin{table}[]
\centering
\begin{tabular}{l  l | l | l | l | l }
From & To &Top $1$ average & Top $10$ average &Top $10\%$ average & Correctness average \\
\hline
$1$ & $9$ & $12.40\%$ & $28.42\%$ & $53.77\%$ & $347.83$\\
$10$ & $29$ & $21.38\%$ & $39.31\%$ & $66.71\%$ & $212.53$ \\
$30$ & $59$ & $28.54\%$ & $47.59\%$ & $75.09\%$ & $162.49$ \\
$60$ & $130$ & $29.57\%$ & $48.90\%$ & $76.56\%$ & $154.45$ 
\end{tabular}
\caption{\footnotesize{Average values table for the Histogram approach (normal conditions).}}
\label{tab:abc}
\end{table}

\subsection{Sampling} \label{results:sampling}

{Sampling consisted in grouping routes from the same user and guessing on them. The procedure was:}

\begin{enumerate}
\item {The Challenger randomly sampled $k$ traces from a single user.}
\item {Sent them to the attacker.}
\item {The attacker responded with a list of candidate users, from most likely to least.}
\end{enumerate}

{Since the challenger had to sample guessing traces, users with less than $k$ were excluded. This was why the $x$ axis of
the graphs were different.}

{In the last subsection, the attacker analysed singular traces. It gave a score regarding its closeness for every user and
sorted all of them accordingly. To calculate the scores for a group of traces, we simply added their scores. For example,
grouping by 2 traces: for the first one, the algorithms assigns the scores $12$, $0$, and $40$ for users $A$, $B$ and
$C$ respectively; for the second one it gives $5$, $30$ and $35$. Adding both values, we had $17$, $30$ and $75$. If lower
score meant higher chances to be the candidate, we would order the users as $A$, $B$ and $C$, $A$ being the top candidate.}

{The results were overall better than the normal approach. We tried to group $2$, $5$ and $10$ traces, with each user being
sampled $10$ times. The more traces we grouped, the better the accuracy. For example, for the Histogram approach, grouping
$2$ traces got a top $1$ accuracy of $40\%$, top $10$ of $60\%$ and top $10\%$ of $80\%$ (for users with a high number of
training traces), much better than checking every trace by itself. For grouping $5$ it had $57\%$, $77\%$ and $90\%$, and
for $10$ they were $71\%$, $87\%$ and $93\%$.}

{Comparing approaches, both versions of the Histogram had better accuracy results than the sequence alignment. Seems like
the scoring system for this attack was not made to combine it with others, since it resulted in an accuracy loss. For
histogram and bayes, adding their scores did not suppose any problem.}

{Naive Bayes was still the best, achieving over $80\%$ accuracy for top $1$ grouping $10$ traces.}

{We also tried to select only a part of the guessing traces (like in \nameref{randomselection}, but only for the testing
part). The users with less traces increased their accuracy, probably due to these being harder to guess on. Since we were
reducing the number of testing traces, there were less different ones, increasing the probabilities of succeeding.}

\begin{figure}[]
\label{fig:his_5s_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_his_samples_5_10.csv_top_graph.png}
\caption[Histogram Accuracy for sampling]{\footnotesize{Histogram accuracy graph (sampling by 5).}}
\end{figure}

\begin{figure}[]
\label{fig:sa_5s_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_sw_samples_5_10.csv_top_graph.png}
\caption[Sequence Alignment for training sessions]{\footnotesize{Sequence Alignment accuracy graph (sampling by 5).}}
\end{figure}

\begin{figure}[]
\label{fig:bayes_5s_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_bayes_samples_5_10.csv_top_graph.png}
\caption[Naive Bayes Accuracy for sampling]{\footnotesize{Bayes accuracy graph (sampling by 5).}}
\end{figure}

\subsection{Same dataset analysis} \label{results:sameds}

{Here, the attacker guessed on the same traces that it trained the model on. As expected, it got better results. A major
element for this could be the fact that, in this case, every user did not visit any domain that was not seen in training.
There was more tolerance for odd behaviour, since in the observation phase all the same sessions were observed.}

{One odd aspect of this environment was that, for the Histogram and the Sequence Alignment attack, we got worst results the
more training traces a user had. This is odd, as we expected it to go up as it had been until that point: with more
training, better results. This could be explained by the model's inability to adapt to a large quantity of traces: with
less traces, the model would be more accurate, rather than for a large number. As to why it did increase in other
scenarios, we suspect that higher training enabled for a better resilience to unknown or odd session features, such as
domains never seen before for a given user.}

{This did not happen for Naive Bayes, as its accuracy went up as usual.}

\begin{figure}[]
\label{fig:his_sameds_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_his_normal_sameds_1.csv_top_graph.png}
\caption[Histogram Accuracy for training sessions]{\footnotesize{Histogram accuracy graph (guessing on training sessions).}}
\end{figure}

\begin{figure}[]
\label{fig:sa_sameds_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_sw_normal_sameds_1.csv_top_graph.png}
\caption[Sequence Alignment Accuracy for training sessions]{\footnotesize{Sequence Alignment accuracy graph (guessing on training sessions).}}
\end{figure}

\subsection{Random selection}  \label{randomselection}

{The selection consisted in the challenger getting only a $k$ percent of all traces before proceeding as usual. For this,
we decided to test for $75\%$ and $50\%$.}

{The graphs' $x$ axis vary for every graph, since the selection was random. We tried to express the information in the
same format for all cases.}

{The sampling created two particular situations: where there were users with no guessing traces and where there were
guessing traces whose user ``owner'' had no training data. On one hand, the first one did not have too much impact by
itself, as it did not include the user in the analysis. On the other hand, the users with no training data caused a
particular problem: how can an attacker guess for a user that did not know that it existed? With our algorithms, we
couldn't, so they were put at the worst candidate position possible.}

{All algorithms results changed, but not considerably. The histogram approaches lost some accuracy, while the others gained
(around $4\%$ of difference). We suspected that these differences came from a ``saturation'' of information, where the
algorithm was incapable of gaining more accuracy with more training. This also happened in other situations: for example,
in the normal condition, the accuracy stagnated when arriving to a certain number of traces. This factor could be the one
determining the minimum number of training sessions to get before performing the attack successfully.}

\subsection{Top domains filter}  \label{topdomains}

{Since the pool of domains was so big, we tried to reduce its number. This way, maybe there could be an improvement in
time-expensive algorithms such as Sequence Alignment. In the dataset, the top 5000 visited domains in Germany were
included, coming from Alexa's web classification. This was because, in the original dataset's paper, it was compared with
the top domains, to see if they correlated.}

{The main idea was to limit the domains to the top entries. Because they were the top domains, losing the rest of them
would not imply a significant loss of information. In practice, however, this was not the case: while limiting to the top
domains did improve significantly the execution time (being $1.7$ times faster than the previous methodology), the
accuracy was significantly worse. The Top 1 and Top 10 averages would drop to around half of its previous value, while the
top $10\%$ dropped a third part.}

{We assumed from these results that less common domains were very important to identify possible users. This made sense, as
at the end of the day the attacker's objective was individuation (identifying the user through the uniqueness of their
profile).}

{These results could be useful for dataset owners: by removing the less common domains, user identification becomes harder.
However, this also comes with a great loss of utility, as it is losing information that is relevant.}

\begin{figure}[]
\label{fig:sa_top500_acc}
\centering
\includegraphics[width=\linewidth]{img/plots/results_sw_normal_top500_1.csv_top_graph.png}
\caption[Sequence Alignment Accuracy for top 500]{\footnotesize{Sequence Alignment accuracy graph (top 500 domains).}}
\end{figure}
