\documentclass[a4paper,12pt]{article}

\input{tools/packages.tex}
\input{tools/styles.tex}
\input{tools/acro.tex}

\usepackage{import}
\usepackage{float}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% COVER %%%
\fancypagestyle{alim}{\fancyhf{}\renewcommand{\headrulewidth}{0pt}
\cfoot{\includegraphics[height=2.2cm]{img/logos/logo_telecos.png}}
}
\thispagestyle{empty}
\begin{center}
{\sffamily 
\resizebox{0.8\textwidth}{!}{\includegraphics{img/logos/upc_completo+telecos.png}}\\
\vspace{1cm}
{\Huge Browsing Behavior - Tracking Users on the Internet}\\
\vspace{0.5cm}
{\color{black}\hrule height 1pt}
\vspace{1cm}
{\large{Master Thesis\\
submitted to the Faculty of the \\
Escola T\`ecnica d'Enginyeria de Telecomunicaci\'o de Barcelona \\
Universitat Polit\`ecnica de Catalunya \\
by\\
\vspace{0.4cm}
Lluís Brosa Hernández}}

\nocite{*} % only if you no use \cite{}

\vspace{1.5cm}

{In partial fulfillment\\
of the requirements for the master in\\
\textbf{CYBERSECURITY}}

\vspace{2cm}

{{Co-advisors: Javier Parra Arnau \& Jordi Forné Muñoz}} \\
{{Barcelona, July 2022}}
\thispagestyle{alim}
}

%%% INDEX %%%
\end{center}
\newpage
\tableofcontents

%%% LISTS %%%
\newpage
\listoffigures
\lstlistoflistings
\listoftables

%%% REVISION %%%
\newpage
\input{revision_history}

%%% ABSTRACT %%%
\clearpage
\newpage
\section*{Abstract}

{Web tracking databases usually use pseudonyms to identify subjects. It has been proven that subject's web behaviour has
  enough information to identify them. In this thesis, we attempted to retrieve this identifying data through simple
  algorithms in an scenario representing a possible real case. The main questions were: how easy it was to link subjects to
  their click sessions using frequency analysis? How many traces were needed to observe before successfully performing the
  attack? Three algorithms were tested: a histogram approach, taking into account domain frequencies; sequence alignment,
  checking sequence order; and Naive Bayes, calculating probabilities. We found that the histogram had a fine accuracy, but
  was surpassed by sequence alignment and Naive Bayes. Sequence alignment was too expensive computationally-wise to use it
  in practice and Naive Bayes was the best in accuracy and good in execution time. The number of sessions required to
  observe for a user were around 30.}

\input{introduction}

\clearpage\section{State of the art}

{This section describes some of the research done in fields related to this project's objective. We focus on two: user
  linkability and predictability. }

\subsection{User Linkability}

{A major privacy concern is the capability of an observing third party to link a user with anonymized information. For
  browsing behaviour, research has been done to see the possible vulnerabilities click trace information can allow.}

{It has been demonstrated that simply applying a pseudonym to web tracking databases is not enough to assure anonymization
  of the dataset subjects. Rather, their browsing behaviour can contain enough information to successfully identify them
  \cite{9152774}.}

{In \cite{trackingbehaviour}, there's a practical attempt of linking users by observing their browsing behaviour, using a
  Naive Bayes classifier with a cosine similarity decision engine. It successfully linked on $88\%$ of its observed
  sessions. In one of the approaches in our project, the Naive Bayes is also used for linking; however, no transformation
  of the data is done here.}

{A practical use of linkability using web behaviour is explained in \cite{10.1145/3038912.3052714}, where they successfully
  linked over $70\%$ of observed user's social media accounts by observing their browsing history. With more information
  available on a user, better accuracy.}


\subsection{User predictability}

{There has been research done about the predictability of users when navigating on the web space. An argument could have
  been made about predictability being important for this project's objective, as predictable users could be easier to
  link to their click sessions.}

{The predictability on the web space can be related to people's mobility patterns. From datasets with geolocated
  information about human movement, scientists were able to study, model and reproduce their structure and regularities.
  Mobility prediction can be useful in traffic forecast, urban pollution, etc. Comparing both physical and virtual
  movement, they are very similar \cite{hazarie2019uncovering}.}

{In the dataset's original paper \cite{https://doi.org/10.48550/arxiv.2012.15112}, a theoretical upper limit for
  predictability of $85\%$ is calculated. The limit mainly varies due to user demographics (gender and age), activities,
  interests and stationarity. Because we were using the dataset that was studied, we could have expected similar limits;
  however, note that this paper was for predicting the next domains a user might visit, not for linking subjects to web
  sessions, and that, because it is the maximum limit, the approaches we experimented on might have not achieved it.}

%%% METHODOLOGY %%%
\clearpage
\section{Project development }

{This section explains the different aspects related to the project's development. Details about the software created are
  explained, such as programming languages used and libraries.}

{The attack scenario designed is explained, with subsections dedicated to different approaches and algorithms an attacker
  would use. Some terms are defined, with their parameters explained.}

\subsection{Software and libraries}

{For the development of the thesis we wrote different programs for data organization and analysis. We tried to use already
  available libraries whenever possible, as they tended to perform better than code made by ourselves \footnote{Python by
    itself is a rather slow programming language compared to compiled languages such as C or C++. The improvements in
    speed can be due to the use of compiled code, as it is the case of Numpy, use of multi-threading, etc. } and were more
  convenient to use, rather than writing code from scratch.}

{The primary language used was the Python programming language \cite{pythonweb}, mainly due to our knowledge and ability to
  use it, and the availability of data analysis libraries.}

{The authors of the dataset used in this project released a specific library to read and parse it. It is written
  in Python as well and heavily depends on the Pandas library, a self-described data analysis and manipulation tool
  \cite{reback2022pandas}. We used it to generate and read the user's traces, as it made it easier to read from and write
  big amounts of data.}

{We also used the Numpy numerical computation library \cite{harris2020array}. For machine-learning related approaches (see
  \nameref{naivebayes}), scikit-learn has a large range of predictive data analysis algorithms \cite{scikit-learn}.}

{The project set up automatically all the required files by downloading the dataset, setting up a virtual environment
  and installing the libraries. A virtual environment in Python can contain libraries installed without conflicting with
  system-wide ones. This can be helpful when there are version conflicts.}

{The main program was able to execute the different attacker approaches with different parameters. These were specified in
  the command line, when executing the program. Features included the possibility to store attack models, multi-threading
  and setting attack situations (explained in \nameref{results}). The results were written in comma-separated values files
  (\path{.csv}).}

{To further analyse the generated information, we first used LibreOffice Calc spreadsheets to generate graphs and tables.
  Because it took too much time to change the spreadsheets data every time we wanted to have another approach, we automated
  the graph generation with MatPlotLib \cite{Hunter:2007} and the tables with Pandas.}

\subsection{Model definition}

{To standardize the different approaches we would be looking at, we defined a challenger/attacker model to base our project
  on:}

\begin{itemize}
\item {The \textbf{challenger} had access to all trajectory data available without being modified nor anonymized.}
\item {The \textbf{attacker} did not have any prior knowledge of any information in the system and had to ask the
  challenger for it.}
\end{itemize}

{The execution consisted on four main stages:}

\begin{enumerate}
\item {The challenger read the click trace data and had it available in memory.}
\item {The attacker asked the challenger for information to build its internal model. It can be described as the
    information required by the algorithm for the following steps. The received trace data had all the fields.}
\item {Afterwards, the attacker asked for data to guess on. This time, the field of users was removed.}
\item {The attacker, through its internal model and some calculations, generated a list of candidates for each trace. They
    were ordered depending on their likeness to be the real user owner. Some users could be positionally tied.}
\item {The challenger verified it by revealing the true subject and its position in the list (defined as its correctness).}
\end{enumerate}

{This format established the strategy followed by the main thesis software. Since we were working with an object-oriented
  programming language, we set a class for the challenger and the attacker. For the attacker, we set an inherited class
  for every method we wanted to try.}

\subsection{Click trace definition}

{We defined a click trace as the sequence of domains visited by a user in a contained period of time. Mainly, it refers to
  a user single web session: for example, someone opens their web browser and starts at Google, then goes to Facebook,
  followed by Youtube, etc.}

{The original dataset did not have a clear way to differentiate between user sessions. For our needs, the
  \path{browsing_with_gap.csv} file had all different domain visits for each user, having their start and end dates. With
  the SequentialNonStationaryTrajectory object from the database library (used in \path{aggregate.py}), we grouped together
  web visits of the same domain to reduce the amount of redundant data. Explanation for trajectory building using the
  library can be read at \cite{https://doi.org/10.48550/arxiv.2012.15112}.}

{After that, domains that were close to each other are grouped together into click traces. The python script
  \path{routes.py} did exactly that: it grouped domains whose time gap between them is within a threshold.}

{Certain parameters were needed to adjust before getting actual results: time between domain visits (gap), minimum time
  for a visit, trace minimum size and trace maximum size. If set incorrectly, traces could be too short or too long:
  figure \ref{fig:traceshis} shows how this changes by varying the gap threshold from $1$ second to $60$ minutes. It can be
  seen how, for larger gaps, their time length and number of domains increased, while the number of traces decreased. At
  the end, the parameters set were:}

\begin{enumerate}
\item {Gap period of $30$ minutes.}
\item {No minimum time for a domain visit, as we don't care if the visit was very short.}
\item {Minimum number of domains for each trace of $2$, since we want to divide it by $2$.}
\end{enumerate}

{With these parameters there were traces distributed for all sizes. These could actually be used for a real case scenario,
  as the gap conforms with the standard definition of a web session \cite{9152774}.}

\begin{figure}[]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{img/traces/th1.png}
    \caption{1 second gap.}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{img/traces/th60.png}
    \caption{60 minutes gap.}
  \end{subfigure}
  \caption[Traces Gap Comparison]{\footnotesize{Trace histograms. Top: traces frequency by duration (seconds); Bottom: frequency by number
      of domains visited.}}
  \label{fig:traceshis}
\end{figure}

{Some sessions were very long, getting to $19$ hours of duration. We though of limiting them to a certain time, but due to
  the program's methodology of modifying traces, we weren't able to. Still, the number of lengthy traces was small, so we
  didn't expect it to affect results in a considerable way.}

{The trace count with these parameters was of $58759$, with $1098$ valid observed users from the original $1141$. Their
  average duration was $65$ minutes, with $27$ domains on average.}

\subsection{Attack approaches}

{This section explains the different algorithms considered for the attacker. For each one of them, a model with available
  data was constructed, followed by guessing on click traces. Each of the proposed methods was unique in these steps,
  unless otherwise specified.}
  
\subsubsection{Histogram} \label{histogram}

{This method consisted on generating an ``average'' histogram for each user based on their traces. The visited domains were
  the factors being counted on, without taking into account the order in which they were seen. To guess on anonymized
  traces, the attacker calculated the distance from the trace's histogram to every user's one.}

{The user's histogram could be considered its ``typical session''. The approach used was to calculate the average
  number of times each domain appeared in a trace. Other classification may also have been made, which is explained
  further below.}

{The first way of calculating the distance for every route was: every domain visited in a trace was compared to
  its corresponding value in the user's histogram. If it did not exist, it was skipped. If none of the domains was in the
  histogram, the candidate was removed. The calculated distance was the sum of all the differences between the trace and
  the user. The candidates list was sorted by their distances in ascending order, with the first users being the most
  likely owners.}

{There was a doubt on users which visited many pages that were not in their histogram. This was complicated to resolve,
  since they might have been visiting websites they had not visited before. The approach explained previously just ignored
  the missing domains, which generated bad results. Another approach was to set these users as ``invalid'' and not taking
  them into account. We ended up rejecting this idea, as this would make the histogram attack the only one with users
  removed. The applied solution was to count the trace histogram value as the difference, only if the domain did not exist
  in the user's histogram. This approach drastically improved the results, so we kept it. All users which did not have any
  known domains would end up tied at the bottom of the candidate classification, so no rejection was needed.}

{Counting all traces as equal is a quite naive approach, as it assumes that every trace is visited under the same
  conditions. Someone might visit different websites depending on the time of day or other factors. We decided to also
  look into classifying traces by time of day and the number of domains, while observing possible improvements compared to
  the original version. We called this variation the ``Advanced Histogram'' approach.}

{By classifying the information to certain conditions, we also tried to address cases where an attacker had a large amount
  of information. In this thesis we were working with a relatively small dataset of over a thousand users. If the attacker
  was a powerful one, with a large dataset from thousands to millions of users (like big tech companies), statistics like
  Top 1 and Top 10 would probably be impossible to achieve.}

{For each user, multiple average histograms were generated. Each training session was classified depending on the time of
  day it started; there were categories such as 'morning' and 'afternoon' to group different times of day. For each one of
  them, a histogram was made depending on their number of domains: for less than or equal to $7$ and another for more than
  $7$. We set the threshold to that number by observing the traces and best determining a quantity that could be considered
  ``large''.}

{For situations where there was not enough information to satisfy all conditions, generic histograms were created. These
  are the same that were explained in the plain histogram attack. When there's a guessing session in a time of day
  that was not seen in the user's training, the generic histogram is used to compare. Otherwise, the histogram of that
  time of day is the one observed.}

{By dividing the data into smaller chunks, the number of candidates could be reduced and have better results. Further
  filtering could have been done, like dividing the traces according to the territory or country it came from (our
  dataset's users were all from Germany).}

% algorithm pseudocode
\include{codi_histogram}

\subsubsection{Naive Bayes} \label{naivebayes}

{Naive Bayes are a set of supervised learning algorithms. They are based on Bayes' theorem, with features being
  independent from each other.}

{Bayes' theorem states the following relationship:}

\begin{equation}\label{bayes1}
  P(A \,|\, B)= \frac{P(B \,|\, A)P(A)}{P(B)}
\end{equation}

{We can use it to calculate the probability of a category given a set of observed values. In our case, the category $u$ is
  a given user and the observed values $d_i$ are the domains in the trace:}

\begin{equation}\label{bayes2}
  P(u \,|\, d_1,d_2,...,d_n)=\frac{P(u)P(d_1,d_2,...,d_n \,|\, u)}{P(d_1,d_2,...,d_n)}
\end{equation}

{For the 'naive' part of the algorithm, we assume that each of the variables is independent from each other. We can then
  simplify the following formula for every value of $i$:}

\begin{equation}\label{bayes3}
  P(d_i\,|\,u,d_1,d_2,...,d_{i-1},d_{i+1},...,d_n) = P(d_i\,|\,u)
\end{equation}

{$P(d_i\,|\,u)$ being the probability of the domain $d_i$ appearing given the observed user $u$. With this, theorem
  \eqref{bayes2} changes to:}

\begin{equation}\label{bayes4}
  P(u \,|\, d_1,d_2,...,d_n)=\frac{P(u)\prod_{i=1}^{n}P(d_i \,|\, u)}{P(d_1,d_2,...,d_n)}
\end{equation}

{We can ignore the denominator, as $P(d_1,d_2,...,d_n)$ is constant. With this, the most likely user is the one with the
  maximum value:}

\begin{equation}\label{bayes5}
  \hat{u} = \arg \underset{y}{\operatorname{max}} \, P(u) \prod_{i=1}^{n}P(d_i\,|\,u)
\end{equation}

{The variation implemented in our case is the Multinomial Naive Bayes, used mainly for text classification. For email
  classification, for example, the observed data would be word counts. For this project, we counted the domains for all
  training traces.}

{The probability of a domain $d_i$ appearing for a user $u$ is calculated as follows:}

\begin{equation}\label{bayes6}
  P(d_i|u) = \frac{N_{u, d_i} + \alpha}{N_{u} + \alpha n}
\end{equation}

{Where $N_{u, d_i}$ is the number of times the domain $d_i$ appeared for user $u$ and $N_u$ is the total number of domains
  for $u$. $\alpha >= 0$ prevents zero probabilities for domains not present in the learning data (we used $\alpha =
  1$).}

{For this method, we ignored the order of domains, as we did in the Histogram-based approach. The implementation was
  straight-foward with scikit-learn; the only code we had to implement was the conversion of the traces to a format the
  library could understand.}

{We feared that the storage needed to store the model would be too high, as we would need to store a value for each domain
  and for each user. However, the model file was not significantly bigger than other approaches, as we only stored the
  domains that a user had visited.}

{For domains that were not observed during training, the library implementation of the algorithm ignores it. The
  possible reason is that, since there was no knowledge of the domain beforehand, every user has the same probability:
  $0+\alpha$. Taking into account this probability for every user would have been redundant, so this is avoided.}

\subsubsection{Sequence alignment-based} \label{sequencealignment}

{The attack approaches explained earlier have in common their 'ignorance' in the trace sequence order. For each domain,
  only the number of appearances it has in a given trace counts. By ignoring their order, there might be a loss of
  potential accuracy that could be achieved.}

{The Sequence alignment approach tries to remedy this by aligning a trace with one sequence modelled after every user.
  Sequence alignment is usually applied in biology to align two DNA sequences to see their similarity. Alignments can be
  done in a 'local' or a 'global' way:}

\begin{itemize}
\item {\textbf{Local alignment} tries to align the sequence in a short way, finding similar regions in sequences that
    differ more.}
\item {\textbf{Global alignment} sets the alignment to the entire sequence.}
\end{itemize}

{Both approaches could have worked here. On one hand, searching for common patterns locally was fine when the traces were
  very different from each other. On the other hand, global alignment might have been the correct choice to see the
  similarities between two traces.}

{In the end, we decided on local alignment. We expected traces to have a great variety of domains and sizes. For
  global alignment, it would have made more sense if the traces were more or less similar. For local, however, if we tried
  to only align for a small part of the sequence, we suspected that we would have been more successful. Comparing global to
  local correlation in practice confirms this, as local tends to perform better.}

{The Smith–Waterman algorithm does local alignment of two sequences \cite{Smith1981}. The process was as follows:}
\begin{enumerate}
\item {Set a gap, an equality and a difference score.}
\item {Create a table of dimensions $n+1$ and $m+1$, where $n$ and $m$ are the length of each sequence. Set the first row
  and column to 0.}
\item {For every empty cell (starting from the most top-left), calculate:}
  \begin{itemize}
  \item {The alignment score: adding the value of the upper-left adjacent cell with its similarity score (if the two
      aligned elements are equal, add the equality score; otherwise, subtract the difference score).}
  \item {Left Gap: subtracting the left adjacent cell value the set gap value}
  \item {Up Gap: same as the last value, but with the upper adjacent cell.}
  \end{itemize}
\item {Set that cell's value to the maximum value calculated of the previous three. If it's a negative value, instead set
    it to $0$.}
\item {After setting all cell values, start from the highest score cell. Go backwards to previous values with the highest
    score until reaching $0$. The followed route is the generated local alignment.}
\end{enumerate}

{For this project, we did not need the actual alignment itself. By only getting a similarity score, it was enough. We set
  the maximum value of the table as the similarity score; we got it by doing all steps except the final one.}

{The score and penalization values were set to define the importance of every match, mismatch or gap:}

\begin{itemize}
\item {Match score: 3}
\item {Mismatch score: -3}
\item {Gap score: -2}
\end{itemize}

{Sequence Alignment is very expensive computational-wise, as the creation of the correlation tables have a complexity
  of $O(nm)$, where $n$ and $m$ are the length of each sequence. Because of this, we had to improve the execution time to
  make it feasible for us to execute.}

{The first approach to this problem was to limit the domains observed to the most visited ones, ranked by Alexa. Although
  it did make the execution faster, its reliability left much to be desired (this is further explained in section
  \ref{topdomains}).}

{Our solution here was to compile our code. We used Cython, a popular conversion library from Python to C
  \cite{behnel2011cython}. Through optimizing python calls and compiling the code, we managed to optimize the program's
  performance a great amount. This did not optimize the algorithm itself: if we were comparing all approaches in an even
  field, this one would still lose, but it made it not as bothersome to execute it.}

\include{codi_sequence}

\input{result}


\clearpage
\section{Conclusions and future development }

{In this thesis, we tried different methods to link subjects with their anonymized web sessions through their web
  behaviour through, designing and implementing software tools. We used the Python programming language, with other
  libraries, to create an interactive program from to which generate multiple tests for all approaches an attacker migh
  perform and for many environmental conditions the attacks may be done under.}

{Through analysis of the generated results, we speculated on the main reasons behind their behaviour. Thanks to this, we
  looked into the possible answer of all questions presented at the introduction.}

{We are satisfied with the testing results, as it shows clear possible ways to link a user with their browsing history.
  All observed algorithms could do linkage, with various degrees of success. For this, we tried simple approaches to
  implement; it would be interesting to see what more complex algorithms (such as machine learning) could do in this
  field.}

{The best algorithm accuracy-wise was Naive Bayes, with high rates of guessing in all situations. Histogram performed fine
  and had the shortest execution time. For Sequence Alignment, it did better than the previous approach, but its complexity
  made it not worth it for practical use. However, it confirmed that trace order matters. If there were to exist better
  local-alignment algorithms complexity-wise, it might surpass the Histogram approach.}

{Recovering one of the questions from the introduction: is frequency analysis a good method to link web behaviour? From
  what the results show, it is. However, it is not from the first considered method where it succeeds (Histogram), but
  instead when the frequency is used as probabilities (Naive Bayes). Since their execution time was rather similar, we
  concluded that Naive Bayes should be used instead, as there were no visible inconveniences to use it over the other
  methods. Even without sequence information, it greately surpassed all other approaches.}

{The minimum number of traces to observe by the attacker to succeed was rather simple. We considered ``success'' here as
  the highest accuracy an attacker could achieve. Through observating the results for all approaches and situations, we
  considered that getting about $30$ sessions might have been enough. Looking at the Naive Bayes approach in a normal case,
  we saw that its accuracy started to stabilyze around that training quantity, with over $50\%$ accuracy. This was mainly
  due to the saturation explained earlier.}

{Further experimentation could be done on algorithm limits: what defines the saturation point for accuracy? Are there other
  features worth analyzing?}

{The dataset we experimented on was rather limited. With over a thousand users, this might have not been a big enough
  sample a more powerful attacker might have, with millions of users visiting these websites. A larger amount of data would
  have let us see with more reliability what happened when there were too many candidates. In the original dataset's paper,
  they also argued about it being limited in just being on one country and one platform, without mobile devices being taken
  into account.}


%%% BIBLIOGRAPHY %%%
\newpage

\medskip
\bibliographystyle{unsrt}
\bibliography{bibliography.bib}

%%% ANNEX %%%
\clearpage
\newpage

\begin{appendices}
  
  \section{Raw and processed data}

  {This thesis includes a \path{.zip} file with all the data that was generated.}

  {Each folder in the root directory correspond to an analysed method. Their names are:}
  
  \begin{itemize}
  \item {\path{his}: \nameref{histogram}}
  \item {\path{his_adv}: Advanced Histogram}
  \item {\path{bayes}: \nameref{naivebayes}}
  \item {\path{sw}: \nameref{sequencealignment} (Smith–Waterman algorithm)}
  \end{itemize}

  {Inside every folder there are comma-separated values files, each with a corresponding folder. The former file is the one
    generated by the main module, while the folders contain information more relevant to the user: plots and data tables.}

  {Their name suffixes correspond to the different situations they were analysed under. For example,
    \path{results_his_samples_10_10} is the Histogram approach under the Sampling situation of 10 traces. The full list
    is:}

  \begin{itemize}
  \item {\path{_normal_1}: \nameref{results:normal}.}
  \item {\path{_samples_2_10}, \path{_samples_5_10} and \path{_samples_10_10}: \nameref{results:sampling} of $2$, $5$ and
      $10$ traces.}
  \item {\path{_samples_5_ktest50_10} and \path{_samples_5_ktest75_10}: Sampling of $5$, with only $50\%$ and $75\%$ of
      guessing click traces.}
  \item {\path{_normal_sameds_1}: \nameref{results:sameds}.}
  \item {\path{_normal_k50_4} and \path{_normal_k75_4}: \nameref{randomselection} of $50\%$ and $75\%$.}
  \item {\path{_normal_top500_1}: \nameref{topdomains}.}
  \end{itemize}
  
  \newpage

  \section{Source code}

  {The source code of the project is available at
    \url{https://gitlab.com/lluisbh/browsing-behavior-tracking-users-on-the-internet}. A copy of the repository is included
    with this thesis.}

  {This program was made to execute in a Linux machine. The \path{python3.8} and \path{gcc-fortran} packages are required
    to be installed beforehand.}

  {To prepare the environment, execute the file \path{install.sh}. It will download the dataset files, create a Python
    virtual environment and install the required libraries. It will install the dataset's library as well.}

  {After it is done, the scripts should be able to execute correctly. To execute everything in order, run
    \path{executeall.sh}. Individually, the scripts used are:}
  \begin{itemize}
  \item {\path{src/aggregate.py}: Create domain visit file, from the information in the dataset.}
  \item {\path{src/routes.py}: Generate user click traces.}
  \item {\path{src/main.py}: Main program, used to test approaches. When run without arguments, it shows its use
      instructions.}
  \item {\path{src/csv_graphs_generator.py}: Generates graphs and information tables. Pass a folder in \path{data/}  as an
      argument (for example, run \path{python3} \path{src/csv_graphs_generator.py} \path{data/his})}
  \end{itemize}

\end{appendices}

\end{document}
