import pandas
from web_tracking.pre_processing.raw_data_processor import RawDataProcessor
from web_tracking.pre_processing.trajectories.sequential_non_stationary_trajectory import SequentialNonStationaryTrajectory

PREPROCESS_FILE = "./data/web_routineness_release/pre_processed/browsing_with_gap.csv"

AGGREGATE_DOMAIN_FILE = "./data/browsing_a_domain.csv"
#AGGREGATE_CATEGORY_FILE = "./data/browsing_a_category.csv"

THRESHOLD = 180

if __name__ == '__main__':
    print("Reading", PREPROCESS_FILE)
    processed = pandas.read_csv(
        PREPROCESS_FILE,
        dtype={'id': int, 'panelist_id': int, 'prev_id': int}, 
        parse_dates=['used_at', 'left_at'],
        converters={
            'category_names'    : lambda text: None if pandas.isnull(text) else tuple(text.split(',')),
            'category_names_top': lambda text: None if pandas.isnull(text) or text == '' else tuple(text.split(',')),
            'category_names_sub': lambda text: None if pandas.isnull(text) or text == '' else tuple(text.split(',')),
        }
    )

    print("Processing")
    RDP = RawDataProcessor(processed)
    RDP.create_time_series(uid='id', 
                           user='panelist_id', 
                           url='url', 
                           domain='domain', 
                           category='category', 
                           starts='used_at', 
                           active_seconds='active_seconds')
    TRAJECTORY_PROCESSED = RDP.df.copy()
    
    print("Aggregating data")
    snst = SequentialNonStationaryTrajectory(TRAJECTORY_PROCESSED)
    snst_domain, = snst.create(threshold=THRESHOLD, features=['domain']).values()
    print(snst_domain.head())
    
    print("Saving at", AGGREGATE_DOMAIN_FILE)
    snst_domain.to_csv(AGGREGATE_DOMAIN_FILE, index=False)
    #print("Saving at", PREPROCESS_FILE)
    #snst_category.to_csv('./data/[4]trajectory_sequential_non_stationary_category.csv', index=False)


