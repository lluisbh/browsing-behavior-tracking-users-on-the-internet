from sys import argv
import time
import pandas
import multiprocessing as mp
import numpy as np
import os

from challenger import Challenger
from attackerhistogram import A_Histogram, A_HistogramAdvanced
from attackerbayes import A_Bayes
from attackeralignment import A_NeedlemanWunsch, A_SmithWaterman

AGGREGATE_DOMAIN_FILE = "./data/browsing_a_domain.csv"
ROUTE_FILE = "./data/routes.csv"
ALEXA_FILE = "./data/web_routineness_release/raw/alexa_GER_top5000.csv"
CORRECT_FILE = "./data/{}/results_{}_{}_{}.csv"

modules = {
        "his" : A_Histogram,
        "his_adv" : A_HistogramAdvanced,
        "bayes" : A_Bayes,
        "nw" : A_NeedlemanWunsch,
        "sw" : A_SmithWaterman,
        }

def print_help():
    print("""
Commands:
\t-m <name>: load module.
\t\this: histogram.
\t\this_adv: histogram advanced.
\t\tbayes: bayes.
\t\tnw: Needleman-Wunsch (global alignment).
\t\tsw: Smith-Waterman (local alignment).
\t-s <count>: sampling approach (default 0).
\t-r <times>: repeat the processing (default 1).
\t-rmodel: load data and do model generation for every repetition.
\t-k <percentage>: % of data to select in total (default 100).
\t-ktest <percentage>: % of data to guess on (default 100).
\t-modelf: attacker model from file.
\t-nocheck: don't check at the end.
\t-sameds: guess on the same training dataset.
\t-top <number>: limits domains to the top visited (according to Alexa).
\t-mt: multithreaded.
""")

def multithread_function(queue, function, args):
    queue.put(function(*args))

def multithread_all(r, function, users=None):
    q = mp.Queue()
    processes = []
    cpus = mp.cpu_count()
    r_mult = np.array_split(r, cpus)
    result = {}
    for cpu in range(cpus):
        #p = mp.Process(target=multithread_function, args=(q, a.classify_users, (r_mult[cpu],users)))
        p = mp.Process(target=multithread_function, args=(q, function, (r_mult[cpu], users)))
        #multithread_function(q, a.classify_users, (r_mult[cpu],users))
        p.start()
        processes.append(p)
    for cpu in range(cpus):
        value_cpu = q.get()
        for k in value_cpu:
            result[k] = value_cpu[k]
    for p in processes:
        p.join()

    return result

if __name__ == '__main__':
    #parsing argv
    loadfile = False
    checkend = True
    sameds = False
    multithread = False
    module_name = ''
    module = None
    sampling = 0
    repeattimes = 1
    repeat_model = False
    method_name = 'normal'
    k = 100
    ktest = 100
    top = 0

    i = 1
    while i < len(argv):
        if argv[i] == '-m' and (i+1) < len(argv):
            i += 1
            if not argv[i] in modules:
                print_help()
                exit(0)
            module_name = argv[i]
            module = modules[module_name]
        elif argv[i] == '-s' and (i+1) < len(argv):
            i += 1
            try:
                sampling = int(argv[i])
            except ValueError:
                print_help()
                exit(0)
            if sampling <= 0:
                print_help()
                exit(0)
            method_name = f'samples_{sampling}'
        elif argv[i] == '-r' and (i+1) < len(argv):
            i += 1
            try:
                repeattimes = int(argv[i])
            except ValueError:
                print_help()
                exit(0)
            if repeattimes <= 0:
                print_help()
                exit(0)
        elif argv[i] == '-k' and (i+1) < len(argv):
            i += 1
            try:
                k = int(argv[i])
            except ValueError:
                print_help()
                exit(0)
            if k <= 0 or k > 100:
                print_help()
                exit(0)
        elif argv[i] == '-ktest' and (i+1) < len(argv):
            i += 1
            try:
                ktest = int(argv[i])
            except ValueError:
                print_help()
                exit(0)
            if ktest <= 0 or ktest > 100:
                print_help()
                exit(0)
        elif argv[i] == '-rmodel':
            repeat_model = True
        elif argv[i] == '-modelf':
            loadfile = True
        elif argv[i] == '-nocheck':
            checkend = False
        elif argv[i] == '-sameds':
            sameds = True
        elif argv[i] == '-mt':
            multithread = True
        elif argv[i] == '-top' and (i+1) < len(argv):
            i += 1
            try:
                top = int(argv[i])
            except ValueError:
                print_help()
                exit(0)
            if top <= 0:
                print_help()
                exit(0)
        else:
            print_help()
            exit(0)
        i += 1
    if module is None:
        print_help()
        exit(0)
    
    if sameds:
        method_name += '_sameds'
    if k != 100:
        method_name += '_k'+str(k)
    if ktest != 100:
        method_name += '_ktest'+str(ktest)
    if top > 0:
        method_name += '_top' + str(top)

    try:
        checks = []
        time_guess = 0
        for i in range(1,repeattimes+1):
            print("Guess",i)
            if i == 1 or repeat_model:
                print('Loading data')
                c = Challenger(ROUTE_FILE, AGGREGATE_DOMAIN_FILE, k/100, ktest/100)
                if top > 0:
                    c.top_only(top, ALEXA_FILE)
                print('Generating model')
                a = module()
                time_model = None
                if loadfile:
                    a.load_model()
                else:
                    time_model = time.process_time()
                    a.generate_model(c.get_known_routes())
                    time_model = time.process_time() - time_model
            print("Guessing")
            if sampling > 0:
                r, users = c.get_sample_user(sampling)
                t = time.perf_counter()
                if multithread:
                    result = multithread_all(r, a.classify_users, users=users)
                else:
                    result = a.classify_users(r, users)
                time_guess += time.perf_counter() - t
            else:
                if sameds:
                    r = c.get_known_routes_anonymized()
                else:
                    r = c.get_missing_routes()
                t = time.perf_counter()
                if multithread:
                    result = multithread_all(r, a.guess_users)
                else:
                    result = a.guess_users(r)
                time_guess += time.perf_counter() - t

            if checkend:
                print('Checking')
                checks.append(c.check_correctness(result))

        if checkend:
            check = pandas.concat(checks)
            try:
                os.mkdir(f'data/{module_name}/')
            except FileExistsError:
                pass
            n = CORRECT_FILE.format(module_name, module_name, method_name, repeattimes)
            check.to_csv(n, index=False)
            print('Output file at', n)
        
        print()
        if not time_model is None:
            print("Model processing time:", time_model)
        if multithread:
            time_guess *= mp.cpu_count()
        print("Guessing time:", time_guess, "s\t", time_guess/repeattimes, "s")

    except KeyboardInterrupt:
        print("Interrupted")
