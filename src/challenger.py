import pandas
import json, ast
import random

class Challenger():
    """
    The class for the Challenger. It reads and filters traces to return them later.
    """
    def __init__(self, routes_path, domains_path, sampling=1, s_test=1):
        """
        Reads trace files.
        """
        self.__routes = pandas.read_csv(
                routes_path,
                dtype={
                    'route': int,
                    'user': int,
                    'timeslot': str,
                    'domains length': int,
                    },
                parse_dates=['starts', 'ends'],
                converters={
                    'duration': lambda l : int(float(l)),
                    'domains': lambda l: ast.literal_eval(l)
                    }
                )

        self.__routes = self.__routes.sample(frac=sampling)

        self.__routes_known = self.__routes
        r_group = self.__routes_known.groupby('user')
        self.__routes_known = r_group.apply(
                lambda df : df.iloc[:len(df)//2].reset_index(drop=True)
                ).reset_index(drop=True)

        self.__routes_missing = self.__routes
        r_group = self.__routes_missing.groupby('user')
        self.__routes_missing = r_group.apply(
                lambda df : df.iloc[len(df)//2:].reset_index(drop=True)
                ).reset_index(drop=True)
        self.__routes_missing = self.__routes_missing.sample(frac=s_test)

    def get_known_routes(self):
        return self.__routes_known

    def get_missing_routes(self):
        return self.__routes_missing.drop('user', axis = 1)

    def get_known_routes_anonymized(self):
        return self.__routes_known.drop('user', axis = 1)

    def get_sample_user(self, count):
        """
        Group traces randomly and return them.
        """
        groups = self.__routes_missing.copy().groupby('user')
        groups = groups.filter(lambda x: len(x) >= count).groupby('user') #remove smaller sets
        df = groups.sample(n=count).reset_index(drop=True) #sample for each user
        users_check = df['user'].unique() #store unique users
        users_check.sort()
        for u in users_check:
            newvalue = random.randrange(10000, 100000000)
            while newvalue in df['user'].values:
                newvalue = random.randrange(10000, 100000000)
            df = df.replace({'user' : u}, newvalue)
        df = df.rename(columns={'user' : 'sample group'})
        return df, users_check

    def top_only(self, top, alexa_path):
        """
        Filter domains so that only the top visited (according to Alexa) remain.
        """
        df_alexa = pandas.read_csv(alexa_path)
        #get top domains
        df_alexa = df_alexa.loc[df_alexa['country_rank'] <= top]
        urls = df_alexa['url'].tolist()
        #filter out domains in self.__routes
        self.__routes['domains'] = [[d for d in domains if d in urls] for domains in self.__routes['domains']]
        #filter out domains in self.__routes_known
        self.__routes_known['domains'] = [[d for d in domains if d in urls] for domains in self.__routes_known['domains']]
        #filter out domains in self.__routes_missing
        self.__routes_missing['domains'] = [[d for d in domains if d in urls] for domains in self.__routes_missing['domains']]
    
    def check_correctness(self, guesses):
        """
        Return the correctness values from the given guesses.
        """
        d = {'route' : [], 'user' : [], 'correctness' : [], 'candidates_len' : [], \
                'domains_len' : [], 'duration' : [], 'training_len' : []}
        groups = self.__routes_known.groupby('user')
        for route_id in guesses:
            #Get original trace
            route = self.__routes.loc[self.__routes['route'] == route_id].iloc[0]
            user = route['user']

            d['route'].append(route_id)
            d['user'].append(user)
            
            for t in guesses[route_id]:
                #Find user
                if t[0] == user:
                    d['correctness'].append(int(t[1]))
                    break
            else:
                d['correctness'].append(len(groups))#max value

            #Other information
            d['candidates_len'].append(len(guesses[route_id]))
            d['domains_len'].append(route['domains length'])
            d['duration'].append(route['duration'])
            if user in groups.groups.keys():
                d['training_len'].append(len(groups.get_group(user)))
            else:
                d['training_len'].append(0)
        return pandas.DataFrame(data = d)

