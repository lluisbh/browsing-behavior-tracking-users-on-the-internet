"""
Store routes for each user.

Classify them by start time.
"""

import pandas
import json

import matplotlib.pyplot as plt

AGGREGATE_DOMAIN_FILE = "./data/browsing_a_domain.csv"
ROUTE_FILE = "./data/routes.csv"

GAP_THRESHOLD = 1800 #1 hour
ROUTE_MINIMUM_TIME = 0 #1 minute
#ROUTE_MAXIMUM_TIME = 10800
ROUTE_MINIMUM_SIZE = 2

if __name__ == '__main__':
    try:
        print("Reading", AGGREGATE_DOMAIN_FILE)
        aggregated = pandas.read_csv(
            AGGREGATE_DOMAIN_FILE,
            dtype={'feat_group': int,
                   'user': int,
                   'cumulative_active_seconds' : int,
                   'gap_seconds' : int
                   }, 
            parse_dates=['starts', 'ends'],
            converters={
                'uids': lambda l: json.loads(l)
            }
        )
        
        print("Aggregation")
        data_out = aggregated.copy()
        data_out = data_out.assign(
            is_user_changed = lambda df: df['user'] != df['user'].shift(),
            gap_seconds = lambda df: (df['starts'] - df['ends'].shift()).dt.total_seconds().fillna(0).apply(int),
            is_threshold_passed = lambda df: df['gap_seconds'] > GAP_THRESHOLD,
            can_be_aggregated = lambda df: df['is_user_changed'] | df['is_threshold_passed'],
            route = lambda df: df['can_be_aggregated'].cumsum(),
        )
        data_out = data_out.groupby('route', as_index=False) \
                           .agg({
                               'domain' : list,
                               'user'    : 'first',
                               'starts'  : 'first',
                               'ends'    : 'last',
                           }) \
                           .rename(columns={'domain': 'domains'})
        
        #Classify to morning, noon, ...
        print("Classifying dates")
        b = [0,4,12,19,24]
        l = ['Late Night', 'Morning', 'Noon', 'Night']
        data_out['timeslot'] = pandas.cut(data_out['starts'].dt.hour, bins = b, labels = l, include_lowest=True)
        
        #Remove very short segments (by time)
        users_len = len(data_out.groupby('user'))
        print("Routes before reduction:", len(data_out))
        data_out = data_out.loc[(data_out['ends'] - data_out['starts']).dt.total_seconds() > ROUTE_MINIMUM_TIME]
        print("After time (short) reduction:", len(data_out))
        
        #Remove very short segments (by quantity)
        g_users = data_out.groupby('user')
        data_out = g_users.filter(lambda x: len(x) >= ROUTE_MINIMUM_SIZE)
        print("After size reduction:", len(data_out))
        print("Number of users before reduction:", users_len)
        print("Number of users after reduction:", len(data_out.groupby('user')))

        #statistics
        time_size = (data_out['ends'] - data_out['starts']).dt.total_seconds()
        data_out['duration'] = time_size
        print()
        print("Time average", time_size.mean())
        print("Time max", time_size.max())
        print("Time min", time_size.min())
        
        domain_size = data_out['domains'].str.len()
        data_out['domains length'] = domain_size
        print()
        print("Domain length average", domain_size.mean())
        print("Domain length max", domain_size.max())
        print("Domain length min", domain_size.min())

        fig, axs = plt.subplots(2)
        axs[0].hist(time_size, bins=200)
        axs[0].set_title("Session Histogram by time")
        axs[0].set_xlabel("Time (seconds)")
        axs[0].set_xlim(0)
        axs[1].hist(domain_size, bins=200)
        axs[1].set_title("Session Histogram by domains")
        axs[1].set_xlabel("Number of domains visited")
        axs[1].set_xlim(0)
        fig.set_figheight(5)
        fig.set_figwidth(6)
        fig.tight_layout()
        plt.show()

    except KeyboardInterrupt:
        print("Interrupted")
    else:
        print("Saving at", ROUTE_FILE)
        data_out.to_csv(ROUTE_FILE, index=False)
