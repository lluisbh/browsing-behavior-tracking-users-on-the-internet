
class Attacker():
    """
    Base class for the attacker.
    """
    def __init__(self, reverse=False):
        self.__reverse = reverse

    def generate_model(self, routes):
        """
        Generates the attacker's model. It should store it in a file. Child classes have to overwrite this.
        """
        pass

    def load_model(self):
        """
        Loads a model from a file. Child classes have to overwrite this.
        """
        pass

    def _process(self, routes, users = None):
        """
        Guess on a set of routes, with the given users. Child classes have to overwrite this.

        Returns a dictionary like {routeid1 : [(userid_1, score), (userid_2, score), ...], ...}. The tuple list has to be 
        sorted by their score value.
        """
        pass

    def guess_users(self, routes, users=None):
        """
        Calls _process method and sets each user's correctness (position at the candidates list, 0 being the most likely 
        candidate).

        Returns a dictionary like {routeid1 : [(userid_1, correctness), (userid_2, correctness), ...], ...}.
        """
        p = self._process(routes, users)
        r = {}
        for k in p:
            r[k] = []
            index = -1
            for i, d in reversed(list(enumerate(p[k]))):
                if index == -1 or p[k][i+1][1] != d[1]:
                    index = i
                r[k].append((d[0],index))
            r[k].reverse()
            #r[k] = [d[0] for d in p[k]]
        return r

    def classify_users(self, routes, users):
        """
        Similar to guess_users, but routes have to be grouped together. This is for the Sampling environment.
        """
        p = self._process(routes, users)
        g = routes.groupby('sample group')
        r = {}
        count = {}
        for sample, df in g:
            routes_df = df[['route']].to_numpy()
            dict_diff = {}
            for route in routes_df:
                for t in p[int(route)]:
                    if not t[0] in dict_diff:
                        dict_diff[t[0]] = 0
                        count[t[0]] = 0
                    dict_diff[t[0]] += t[1]
                    count[t[0]] += 1

            sorted_results = sorted(dict_diff.items(), reverse=self.__reverse, key=lambda i: i[1])

            r[int(routes_df[0])] = [(k,v) for k,v in sorted_results if count[k] >= len(routes_df)]

        e = {}
        for k in r:
            e[k] = []
            index = -1
            for i, d in reversed(list(enumerate(r[k]))):
                if index == -1 or r[k][i+1][1] != d[1]:
                    index = i
                e[k].append((d[0],index))
            e[k].reverse()
            #r[k] = [d[0] for d in p[k]]

        return e
