import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import os
from sys import argv

def print_help():
    print("""
Program to read generated csv files and generate relevant data.
Commands:
\t<folder>: folder to read (mandatory)
    """)

if __name__ == '__main__':
    if len(argv) < 2:
        print_help()
        exit(0)
    try:
        for file_path in os.listdir(argv[1]):
            if file_path.endswith(".csv"): 
                print("Reading file", file_path)
                try:
                    os.mkdir(os.path.join(argv[1], file_path[:-4]))
                except FileExistsError:
                    pass
                full_path = os.path.join(argv[1], file_path)
                new_path = os.path.join(argv[1], file_path[:-4], file_path)
                #Original data
                df = pd.read_csv(full_path)
                #Overall information
                overall = {
                        "user" : [],
                        "Total" : [],
                        "Training" : [],
                        "Correct average" : [],
                        "Domain length average" : [],
                        "Duration average" : [],
                        "Top 1" : [],
                        "Top 10" : [],
                        "Top 10%" : [],
                        "Top 1 (relative)" : [],
                        "Top 10 (relative)" : [],
                        "Top 10% (relative)" : [],
                        }
                
                users = df.groupby("user")
                for user, df_user in users:
                    overall["user"].append(user)
                    overall["Total"].append(len(df_user))
                    overall["Training"].append(df_user["training_len"].iloc[0])
                    overall["Correct average"].append(df_user["correctness"].mean())
                    overall["Domain length average"].append(round(df_user["domains_len"].mean()))
                    overall["Duration average"].append(round(df_user["duration"].mean()))
                    overall["Top 1"].append(len(df_user[df_user["correctness"] < 1]))
                    overall["Top 10"].append(len(df_user[df_user["correctness"] < 10]))
                    overall["Top 10%"].append(len(df_user[df_user["correctness"] < (df_user["candidates_len"]*0.10)]))
                    overall["Top 1 (relative)"].append(overall["Top 1"][-1] / len(df_user))
                    overall["Top 10 (relative)"].append(overall["Top 10"][-1] / len(df_user))
                    overall["Top 10% (relative)"].append(overall["Top 10%"][-1] / len(df_user))
                df_overall = pd.DataFrame(overall)
                #Training grouping
                df_training = df_overall.groupby('Training').agg("mean").drop(['user','Total'], axis=1).reset_index()
                #Length grouping
                df_domainlen = df_overall.groupby('Domain length average').agg("mean").drop(['user','Total', 'Training'], axis=1).reset_index()

                #Accuracy table
                accuracy = {
                        "From" : [],
                        "To" : [],
                        "Top 1 average" : [],
                        "Top 10 average" : [],
                        "Top 10% average" : [],
                        "Correctness average" : [],
                        "STD Top 1": [],
                        "STD Top 10": [],
                        "STD Top 10%": [],
                        "STD Correctness": [],
                        }
                range_fractions = (1,10,30,60,df_overall["Training"].max()+1)
                value = range_fractions[0]
                for r in range_fractions[1:]:
                    accuracy["From"].append(value)
                    accuracy["To"].append(r-1)
                    selection = df_overall[df_overall["Training"] < r] 
                    selection = selection[selection["Training"] >= value]
                    accuracy["Top 1 average"].append(selection["Top 1 (relative)"].mean())
                    accuracy["Top 10 average"].append(selection["Top 10 (relative)"].mean())
                    accuracy["Top 10% average"].append(selection["Top 10% (relative)"].mean())
                    accuracy["Correctness average"].append(selection["Correct average"].mean())
                    accuracy["STD Top 1"].append(selection["Top 1 (relative)"].std())
                    accuracy["STD Top 10"].append(selection["Top 10 (relative)"].std())
                    accuracy["STD Top 10%"].append(selection["Top 10% (relative)"].std())
                    accuracy["STD Correctness"].append(selection["Correct average"].std())
                    value = r
                df_accuracy = pd.DataFrame(accuracy)

                print("Writing output...")
                with pd.ExcelWriter(new_path + '_processed.xlsx') as ew:
                    df.to_excel(ew, sheet_name='Original', index=False)
                    df_overall.to_excel(ew, sheet_name='Overall', index=False)
                    df_accuracy.to_excel(ew, sheet_name='Accuracy', index=False)

                df_sorted = df_training.sort_values(
                        by=["Training", "Top 10% (relative)", "Top 10 (relative)", "Top 1 (relative)", "Correct average"], 
                        ignore_index = True)
                
                #top graph
                fig = plt.figure(figsize=(6.4*2, 4.8))
                SPLIT = max(df_sorted['Training'].iloc[-1]//35, 2)
                y = np.average(df_sorted['Top 10% (relative)'].to_numpy()\
                        [:len(df_sorted['Top 10% (relative)'])-len(df_sorted['Top 10% (relative)'])%SPLIT]\
                        .reshape(-1, SPLIT), axis=1)
                if len(df_sorted['Training']) % SPLIT != 0:
                    y = np.append(y, np.average(df_sorted['Top 10% (relative)'].to_numpy()\
                            [len(df_sorted['Top 10% (relative)'])-len(df_sorted['Top 10% (relative)'])%SPLIT]))
                    y = np.append(y, y[-1])
                x = range(len(y))
                plt.fill_between(x, y, color='#004586', label="Top 10%", step='post')
                y = np.average(df_sorted['Top 10 (relative)'].to_numpy()\
                        [:len(df_sorted['Top 10 (relative)'])-len(df_sorted['Top 10 (relative)'])%SPLIT]\
                        .reshape(-1, SPLIT), axis=1)
                if len(df_sorted['Training']) % SPLIT != 0:
                    y = np.append(y, np.average(df_sorted['Top 10 (relative)'].to_numpy()\
                            [len(df_sorted['Top 10 (relative)'])-len(df_sorted['Top 10 (relative)'])%SPLIT]))
                    y = np.append(y, y[-1])
                plt.fill_between(x, y, color='#ffd320', label="Top 10", step='post')
                y = np.average(df_sorted['Top 1 (relative)'].to_numpy()\
                        [:len(df_sorted['Top 1 (relative)'])-len(df_sorted['Top 1 (relative)'])%SPLIT]\
                        .reshape(-1, SPLIT), axis=1)
                if len(df_sorted['Training']) % SPLIT != 0:
                    y = np.append(y, np.average(df_sorted['Top 1 (relative)'].to_numpy()\
                            [len(df_sorted['Top 1 (relative)'])-len(df_sorted['Top 1 (relative)'])%SPLIT]))
                    y = np.append(y, y[-1])
                plt.fill_between(x, y, color='#ff420e', label="Top 1", step='post')
                plt.axis([0,len(x)-1, 0, 1])
                ticks = df_sorted['Training'][0::SPLIT].tolist()
                if len(df_sorted['Training']) % SPLIT != 0:
                    ticks = np.append(ticks, df_sorted['Training'].iloc[-1])
                plt.xticks(x, ticks)
                plt.grid()
                plt.legend()
                ax = fig.get_axes()[0]
                ax.set_title("Accuracy by Training")
                ax.set_xlabel("Quantity of training sessions")
                ax.set_ylabel("Percentage")
                ax.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1.0))
                plt.savefig(new_path + '_top_graph.png', format='png')
                #plt.show()
                plt.close()

                #correctness graph
                fig = plt.figure(figsize=(6.4*2, 4.8))
                x = range(len(df_sorted['Training']))
                y = df_sorted['Correct average']
                plt.fill_between(x, y, color='#004586', label="Correctness average")
                plt.axis([0,len(x), 0, 1200])
                plt.xticks(range(0, len(df_sorted['Training']),SPLIT), df_sorted['Training'][0::SPLIT].tolist())
                plt.grid()
                plt.legend()
                ax = fig.get_axes()[0]
                ax.set_title("Correctness/Training traces")
                ax.set_xlabel("Quantity of training sessions")
                ax.set_ylabel("Correctness")
                plt.savefig(new_path + '_correctness.png', format='png')
                #plt.show()
                plt.close()

                #domains/correctness (average)
                SPLIT = 4
                df_sorted = df_domainlen.sort_values(
                        by=["Domain length average", "Top 10% (relative)", "Top 10 (relative)", "Top 1 (relative)", "Correct average"], 
                        ignore_index = True)
                fig = plt.figure(figsize=(6.4*2, 4.8))
                x = range(len(df_sorted['Domain length average']))
                y = df_sorted['Correct average']
                plt.fill_between(x, y, color='#004586', label="Correctness average")
                plt.axis([0,len(x), 0, 1200])
                plt.xticks(range(0, len(df_sorted['Domain length average']),SPLIT), df_sorted['Domain length average'][0::SPLIT].tolist())
                plt.grid()
                plt.legend()
                ax = fig.get_axes()[0]
                ax.set_title("Correctness/Domain length comparison (average)")
                ax.set_xlabel("Number of domains")
                ax.set_ylabel("Correctness")
                plt.savefig(new_path + '_correctness_domains_avg.png', format='png')
                #plt.show()
                plt.close()

    except KeyboardInterrupt:
        print("Interrupted")
