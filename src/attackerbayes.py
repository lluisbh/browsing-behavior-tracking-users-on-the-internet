from attacker import Attacker
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction import DictVectorizer
import numpy as np
import json

class A_Bayes(Attacker):
    """
    An attacker which uses the Naive Bayes approach.
    """

    MODEL_DIR = 'data/models/bayes.dat'

    def __init__(self):
        super().__init__(reverse=True)

    def generate_model(self, routes):
        data = {}
        for r in routes.itertuples():
            if r.user not in data:
                data[r.user] = {}
            for d in r.domains:
                if d not in data[r.user]:
                    data[r.user][d] = 0
                #Count frequency of domain for each user
                data[r.user][d] += 1
        
        X = []
        y = []
        for user in data:
            y.append(user)
            X.append(data[user])
        
        #Setup the class model with the collected information
        self.__vectorizer = DictVectorizer() 
        X = self.__vectorizer.fit_transform(X)
        self.__model = MultinomialNB().fit(X, y)
        self.__target_values = y

        with open(self.MODEL_DIR, 'w') as f:
            f.write(json.dumps(data))

    def load_model(self):
        with open(self.MODEL_DIR, 'r') as f:
            data = json.loads(f.read())

        X = []
        y = []
        for user in data:
            y.append(int(user))
            X.append(data[user])
        
        self.__vectorizer = DictVectorizer() 
        X = self.__vectorizer.fit_transform(X)
        self.__model = MultinomialNB().fit(X, y)
        self.__target_values = y

    def _process(self, routes, users = None):
        progress = 0

        result = {}

        c = self.__vectorizer.get_feature_names_out()

        routes_order = []
        data = []

        for r in routes.itertuples():
            routes_order.append(r.route)

            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)

            progress += 1

            part = {}

            for d in r.domains:
                if d not in data:
                    part[d] = 0
                #Count the domain frequency for this click trace
                part[d] += 1
            data.append(part)

        v = DictVectorizer()
        X = self.__vectorizer.transform(data)

        print("Probabilities")
        #Calculate probavbilities through the model class
        p = self.__model.predict_proba(X)
        
        print("ending...")
        j = 0
        #Sorting the results
        if users is None:
            for r in routes_order:
                result[r] = [(self.__target_values[i], p[j][i]) for i in range(len(p[0]))]
                result[r].sort(reverse = True, key = lambda t: t[1])
                j += 1
        else:
            for r in routes_order:
                result[r] = [(self.__target_values[i], p[j][i]) for i in range(len(p[0])) \
                        if self.__target_values[i] in users]
                result[r].sort(reverse = True, key = lambda t: t[1])
                j += 1

        return result

