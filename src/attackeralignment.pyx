#cython: language_level=3
from attacker import Attacker
cimport cython

import numpy as np
import json

class A_SequenceAlignment(Attacker):
    """
    Base class for Sequence Alignment approaches.
    """
    PATTERNS_COUNT = 10

    def __init__(self):
        super().__init__(reverse=True)

    def matching(self, seq1, seq2, int match_s=1, int mismatch_s=-1, int gap_s=-1):
        """
        Function for the alignment algorithm.
        """
        pass

    def generate_model(self, routes):
        """
        1. Count times a pattern appears
        2. Find X most repeated patterns
        """
        count = {}
        cdef int progress = 0

        for route in routes.itertuples():
            if len(routes) < 200 or progress % (len(routes)//200) == 0:
                print(int(progress/len(routes)*50), '%', end='\r', flush=True)
            progress += 1

            if route.user not in count:
                count[route.user] = {}
            domains = tuple(route.domains)
            if domains not in count[route.user]:
                count[route.user][domains] = 0
            #Count frequency of these domain appearing in this order
            count[route.user][domains] += 1

        model = {}

        for user in count:
            if len(routes) < 200 or progress % (len(routes)//200) == 0:
                print(int(progress/len(routes)*50), '%', end='\r', flush=True)
            progress += 1

            keys_sorted = sorted(count[user].keys(), key=lambda x: count[user].get(x), reverse=True)

            #Get the most frequent domains order
            if len(count[user]) > self.PATTERNS_COUNT:
                max_values = keys_sorted[:self.PATTERNS_COUNT]
            else:
                max_values = keys_sorted
                while len(max_values) < self.PATTERNS_COUNT:
                    max_values.append(('.'))
            model[user] = max_values
        
        self.__model = model

        print("100%")

        with open(self.MODEL_DIR, 'w') as f:
            f.write(json.dumps(model))

    def load_model(self):
        with open(self.MODEL_DIR, 'r') as f:
            m = json.loads(f.read())
        self.__model = {}
        for u in m:
            self.__model[int(u)] = m[u]

    def _process(self, routes, users = None):
        result = {}
        cdef int progress = 0
        cdef int d

        for route in routes.itertuples():
            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)
            progress += 1

            differences = []
            for user in self.__model:
                if not users is None and not user in users:
                    continue
                d = 0
                for proposal in self.__model[user]:
                    #Get algorithm score for each user and each model value
                    d += self.matching(proposal, route.domains, 3, -3, -2)
                differences.append((user, d))
            differences.sort(key = lambda t: t[1], reverse=True)
            result[route.route] = differences

        return result

class A_NeedlemanWunsch(A_SequenceAlignment):
    MODEL_DIR = 'data/models/neddleman_wunsch.dat'

    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def matching(self, seq1, seq2, int match_s=1, int mismatch_s=-1, int gap_s=-1):
        """
        Needleman-Wunsch algorithm
        Seq1 and seq2 are array-like objects with ordered elements.

        Returns last cell value
        """
        #matrix creation
        cdef int s1_len = len(seq1)
        cdef int s2_len = len(seq2)
        cdef long[:,:] matrix = np.empty((s1_len+1, s2_len+1), dtype=np.int_)
        cdef int i, v

        #matrix[0] = np.arange(0, s2_len*gap_s-1, gap_s)
        for i,v in enumerate(np.arange(0, s2_len*gap_s-1, gap_s)):
            matrix[0,i] = v
        
        #matrix[:,0] = np.arange(0, s1_len*gap_s-1, gap_s)
        for i,v in enumerate(np.arange(0, s1_len*gap_s-1, gap_s)):
            matrix[i,0] = v
    
        #iteration
        cdef int j
        cdef int m
        cdef int left_val, upleft_val, up_val
        for i, row in enumerate(matrix[1:]):
            left_val = row[0]
            upleft_val = matrix[i,0]
            for j in range(len(row[1:])):
                up_val = matrix[i,j+1] 

                if seq1[i] == seq2[j]:
                    m = match_s
                else:
                    m = mismatch_s
                matrix[i+1,j+1] = max(upleft_val+m, left_val+gap_s, up_val+gap_s)

                left_val = matrix[i+1,j+1]
                upleft_val = up_val

        return matrix[s1_len, s2_len]


class A_SmithWaterman(A_SequenceAlignment):
    MODEL_DIR = 'data/models/smith_waterman.dat'

    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def matching(self, seq1, seq2, int match_s=1, int mismatch_s=-1, int gap_s=-1):
        """
        Smith-Waterman algorithm
        Seq1 and seq2 are array-like objects with ordered elements.

        Returns last cell value
        """
        #matrix creation
        cdef int s1_len = len(seq1)
        cdef int s2_len = len(seq2)
        cdef long[:,:] matrix = np.zeros((s1_len+1, s2_len+1), dtype=np.int_)

        #iteration
        cdef int i, j
        cdef int m
        cdef int left_val, upleft_val, up_val
        for i, row in enumerate(matrix[1:]):
            left_val = row[0]
            upleft_val = matrix[i,0]
            for j in range(len(row[1:])):
                up_val = matrix[i,j+1] 

                if seq1[i] == seq2[j]:
                    m = match_s
                else:
                    m = mismatch_s
                matrix[i+1,j+1] = max(0, upleft_val+m, left_val+gap_s, up_val+gap_s)

                left_val = matrix[i+1,j+1]
                upleft_val = up_val

        return np.amax(matrix)

