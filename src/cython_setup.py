#python setup.py build_ext --inplace

from distutils.core import setup
from Cython.Build import cythonize

setup(name='Attacker Alignment Cython',
      ext_modules=cythonize("attackeralignment.pyx"))
