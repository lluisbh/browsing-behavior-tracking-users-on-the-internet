from attacker import Attacker
import json

class A_Histogram(Attacker):
    """
    An attacker which uses the Histogram approach.
    """

    MODEL_DIR = 'data/models/histogram.dat'

    def generate_model(self, routes):
        #{user: {domain1: count, d2: c2, ...}, ...}
        data = {}
        progress = 0

        for r in routes.itertuples():
            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)

            progress += 1

            if not r.user in data:
                data[r.user] = {}

            for d in r.domains:
                if not d in data[r.user]:
                    data[r.user][d] = 0
                #Count frequency of domain for each user
                data[r.user][d] += 1
        print('100%')

        g = routes.groupby('user')
        for user, df in g:
            for key in data[user]:
                #Calculate average
                data[user][key] = data[user][key] / len(df.index)

        with open(self.MODEL_DIR, 'w') as f:
            f.write(json.dumps(data))

        self.__model = data

    def load_model(self):
        with open(self.MODEL_DIR, 'r') as f:
            m = json.loads(f.read())
        self.__model = {}
        for u in m:
            self.__model[int(u)] = m[u]

    def _process(self, routes, users = None):
        progress = 0

        result = {}

        for r in routes.itertuples():
            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)

            progress += 1

            #read domain routes
            r_dict = {}
            for d in r.domains:
                if not d in r_dict:
                    r_dict[d] = 0
                r_dict[d] += 1
            
            #compare with users
            differences = []
            for user in self.__model:
                if not users is None and not user in users:
                    continue #skip user

                d = 0.0
                for domain in r_dict:
                    if domain in self.__model[user]:
                        d += abs(self.__model[user][domain] - r_dict[domain])
                    else:
                        d += r_dict[domain]
                
                differences.append((user, d))

            differences.sort(key = lambda t: t[1])
            result[r.route] = [(int(d[0]), d[1]) for d in differences]
        print('100%')

        return result
    
class A_HistogramAdvanced(Attacker):
    """
    Alternative version of the Histogram approach, in which further classification is applied.
    """

    MODEL_DIR = 'data/models/histogram_advanced.dat'

    def generate_model(self, routes):
        #{user: [{day: {domain1: count, d2: c2, ...}, night: {...}, ...},  {...}], ...}
        data = {}
        progress = 0

        for r in routes.itertuples():
            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)

            progress += 1

            if not r.user in data:
                data[r.user] = [{},{},{}]
                data[r.user][0]['default'] = {"total" : 0} #0: <=7
                data[r.user][1]['default'] = {"total" : 0} #1: >7
                data[r.user][2]['default'] = {"total" : 0} #2: total
            if not r.timeslot in data[r.user][2]:
                data[r.user][0][r.timeslot] = {"total" : 0}
                data[r.user][1][r.timeslot] = {"total" : 0}
                data[r.user][2][r.timeslot] = {"total" : 0}

            if len(r.domains) <= 7:
                i = 0
            else:
                i = 1

            data[r.user][i][r.timeslot]["total"] += 1
            data[r.user][i]['default']["total"] += 1
            data[r.user][2][r.timeslot]["total"] += 1

            for d in r.domains:
                if not d in data[r.user][i][r.timeslot]:
                    data[r.user][i][r.timeslot][d] = 0
                if not d in data[r.user][i]['default']:
                    data[r.user][i]['default'][d] = 0
                data[r.user][i][r.timeslot][d] += 1
                data[r.user][i]['default'][d] += 1
                if not d in data[r.user][2][r.timeslot]:
                    data[r.user][2][r.timeslot][d] = 0
                if not d in data[r.user][2]['default']:
                    data[r.user][2]['default'][d] = 0
                data[r.user][2][r.timeslot][d] += 1
                data[r.user][2]['default'][d] += 1
        print('100%')

        g = routes.groupby('user')
        for user, df in g:
            for domain in data[user][2]['default']:
                data[user][2]['default'][domain] = data[user][2]['default'][domain] / len(df.index)
            for ts, dff in df.groupby('timeslot'):
                for i in range(2):
                    if ts in data[user][i]:
                        for domain in data[user][i][ts]:
                            if domain != 'total':
                                data[user][i][ts][domain] = data[user][i][ts][domain] / data[user][i][ts]['total']
                    for domain in data[user][i]['default']:
                        if domain != 'total':
                            data[user][i]['default'][domain] = data[user][i]['default'][domain] / data[user][i]['default']['total']
        
        with open(self.MODEL_DIR, 'w') as f:
            f.write(json.dumps(data))

        self.__model = data

    def load_model(self):
        with open(self.MODEL_DIR, 'r') as f:
            m = json.loads(f.read())
        self.__model = {}
        for u in m:
            self.__model[int(u)] = m[u]

    def _process(self, routes, users = None):
        progress = 0

        result = {}

        for r in routes.itertuples():
            if len(routes) < 100 or progress % (len(routes)//100) == 0:
                print(int(progress/len(routes)*100), '%', end='\r', flush=True)

            progress += 1

            #read domain routes
            r_dict = {}
            for d in r.domains:
                if not d in r_dict:
                    r_dict[d] = 0
                r_dict[d] += 1
            
            #classification by size
            if len(r.domains) <= 7:
                i = 0
            else:
                i = 1

            #compare with users
            differences = []
            for user in self.__model:
                if not users is None and not user in users:
                    continue #skip user
                d = 0.0
                for domain in r_dict:
                    if r.timeslot in self.__model[user][i] and domain in self.__model[user][i][r.timeslot]:
                        d += abs(self.__model[user][i][r.timeslot][domain] - r_dict[domain])
                    elif domain in self.__model[user][i]['default']:
                        d += abs(self.__model[user][i]['default'][domain] - r_dict[domain])
                    elif domain in self.__model[user][2]['default']:
                        d += abs(self.__model[user][2]['default'][domain] - r_dict[domain])
                    else:
                        d += r_dict[domain]
                
                differences.append((user, d))

            differences.sort(key = lambda t: t[1])
            result[r.route] = [(int(d[0]), d[1]) for d in differences]
        print('100%')

        return result
    
