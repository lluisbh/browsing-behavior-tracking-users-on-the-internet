#/bin/sh

PYTHON=python3.8
VENV_PATH=venv

WEBTRACKING_GIT=https://github.com/gesiscss/web_tracking
WEBTRACKING_NAME=web_tracking

DATA_DIR=data
WEBTRACKING_DATA=https://zenodo.org/record/4757574/files/web_tracking_data.tar.gz
WEBTRACKING_DATA_FILE=web_tracking_data.tar.gz

echo "Creating Virtual Environment..."
${PYTHON} -m venv ${VENV_PATH}

echo "Downloading data..."
mkdir ${DATA_DIR}
mkdir ${DATA_DIR}/models
wget ${WEBTRACKING_DATA}
tar -xvzf ${WEBTRACKING_DATA_FILE} -C ${DATA_DIR}
rm ${WEBTRACKING_DATA_FILE}

echo "Installing python libraries"
git clone ${WEBTRACKING_GIT}
cd ${WEBTRACKING_NAME}
../${VENV_PATH}/bin/python3 setup.py install
cd ..
${VENV_PATH}/bin/pip install -r requirements.txt
rm -rf ${WEBTRACKING_NAME}

#compile cython files
cd src/
../${VENV_PATH}/bin/python3 cython_setup.py build_ext --inplace
cd ..
